import axios from 'axios';
import * as actionTypes from '../constants/galleryConst';


const getDataRequested = () => {
    return {
        type: actionTypes.GET_DATA_REQUESTED
    };
};

const setRateRequested = () => {
    return {
        type: actionTypes.SET_RATE_REQUESTED
    };
};

const getGalleryDataDone = data => {
    return {
        type: actionTypes.GET_PHOTOS_SUCCESS,
        data
    };
};

const getDataFailed = () => {
    return {
        type: actionTypes.GET_PHOTOS_ERROR
    };
};

export const getDataItemDone = data => {
    return {
        type: actionTypes.GET_DATA_ITEM,
        data
    };
};

const setGalleryItemRateDone = data => {
    return {
        type: actionTypes.SET_PHOTOS_RATE_SUCCESS,
        data
    };
};

export const getGalleryData = (id) => dispatch => {
    const url = '/photos';
    dispatch(getDataRequested());

    axios.get(url)
        .then(res => {
            const data = res.data;
            dispatch(getGalleryDataDone(data));
        })
        .catch(error => {
            dispatch(getDataFailed(error));
        })
};

export const setGalleryItemRate = (body) => dispatch => {
    const url = '/setRate';
    dispatch(setRateRequested());
    axios.put(url, body)
        .then(res => {
            dispatch(setGalleryItemRateDone(body));
        })
        .catch(error => {
            dispatch(getDataFailed(error));
        })
};