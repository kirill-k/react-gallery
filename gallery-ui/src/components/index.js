import React from 'react';
import GalleryComponent from './gallery'
import '../index.css';


const RootComponent = () => {

    return(
        <div className="wrapper">
            <h1>Photo Gallery</h1>
            <GalleryComponent />
        </div>
    )
};


export default RootComponent;
