import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ModalWrappedComponent from '../common/modal';
import SelectRate from './rateSelector';
import './index.css';


class ModalRate extends Component {

    render() {
        const { data, setGalleryItemRate } = this.props;

        return (
            <div className="descriptionContainer">
                <img
                    className="fullImage"
                    src={data.url}
                    alt={data.title}
                />
                <p>{data.description}</p>
                <SelectRate
                    id={data.id}
                    rate={data.rate}
                    setGalleryItemRate={setGalleryItemRate}
                />
            </div>
        )
    }
}


ModalRate.propTypes = {
    data: PropTypes.object.isRequired
};

export default ModalWrappedComponent(ModalRate, {showCloseButton: true} )