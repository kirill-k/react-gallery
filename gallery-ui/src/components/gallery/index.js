import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loader from '../common/loader/'
import ModalRate from './modal'
import { bindActionCreators } from 'redux'
import * as galleryActions from '../../actions/galleryActions'
import { connect } from 'react-redux';
import './index.css';


class GalleryComponent extends Component {
    state = { showModal: false }

    componentDidMount() {
        const { actions } = this.props;
        actions.getGalleryData();
    };

    handleModalShow = (event, item) => {
        event.persist();
        const { actions } = this.props;

        actions.getDataItemDone(item);
        actions.getGalleryData();
        this.setState({ showModal: !this.state.showModal })
    };

    render() {
        const { gallery, actions } = this.props;
        const { showModal } = this.state;

        return (
            <div className="container">
                {showModal &&
                    <ModalRate
                        data={gallery.photoData}
                        loading={gallery.loading}
                        setGalleryItemRate={actions.setGalleryItemRate}
                        handleToggleModal={this.handleModalShow}
                        showModal={showModal}
                    />
                }
                { gallery.loading ? <Loader /> :
                    gallery.photos.map(item =>
                        <div
                            className="imgBlock"
                            key={item.id}
                            onClick={(event) => this.handleModalShow(event, item)}
                        >
                            <h3>{item.title}</h3>
                            <div>
                                <img className="thumbnailsImg" src={item.url} alt={item.name}/>
                                <div>{`Published: ${item.date}`}</div>
                                <div>{`Rate: ${item.rate}`}</div>
                            </div>
                        </div>
                    )
                }
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        gallery: state
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(galleryActions, dispatch)
    };
};

GalleryComponent.propTypes = {
    gallery: PropTypes.object
};


export default connect(mapStateToProps, mapDispatchToProps)(GalleryComponent);