import React, { Component } from 'react';
import PropTypes from 'prop-types';


class SelectRate extends Component {
    state = { localRate: 1 };

    renderRateOptions = () => {
        let rateOptions = [];

        for(let i=1; i<=5; i++){
            rateOptions.push(i);
        }
        return rateOptions.map(elem => (
            <option key={elem} value={elem}>{elem}</option>
        ))
    };

    handleChangeRate = (e) => {
        this.setState({
            localRate: e.target.value
        })
    };

    handleSubmit = () => {
        const { id, setGalleryItemRate } = this.props;
        setGalleryItemRate({ id, rate: this.state.localRate })
    };

    render() {
        const { rate } = this.props;
        const { localRate } = this.state;

        return (
            <div>
                <select value={localRate} onChange={this.handleChangeRate} >
                    {this.renderRateOptions()}
                </select>
                <span>{`Rate ${rate}`}</span>
                <button onClick={this.handleSubmit}>Rate the photo</button>
            </div>

        );
    }
}

SelectRate.propTypes = {
    id: PropTypes.number,
    rate: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    setGalleryItemRate: PropTypes.func
};


export default SelectRate;