import React from 'react'
import './index.css'

export default (ModalWrappedComponent, {
    closeCallbackName = '',
    showCloseButton = false,
    closeButton = <div className='closeButton' />,
    closeOnBackdropClick = true,
    isFixed = true,
    backdropOpacity = 0.4,
}) => props =>
    <div>
        <div
            className='backdrop'
            style={{opacity: backdropOpacity}}
            onClick={props.handleToggleModal}
        />
        <div className={`modalWrapContainer ${isFixed && 'isFixed'}`}>
            { showCloseButton && <div onClick={props.handleToggleModal} >{closeButton}</div> }
            <ModalWrappedComponent {...props} />
        </div>
    </div>