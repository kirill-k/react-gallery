import React from 'react'


const Loader = () => {
    const blockStyles = {
        display: 'flex',
        justifyContent: 'center'
    };
    return(
        <div style={blockStyles}>
            <div>Loading...</div>
        </div>
    )
};


export default Loader;