import {
    GET_DATA_REQUESTED,
    GET_PHOTOS_SUCCESS,
    GET_PHOTOS_ERROR,
    GET_DATA_ITEM,
    SET_RATE_REQUESTED,
    SET_PHOTOS_RATE_SUCCESS
} from '../constants/galleryConst';

const initialState = {
    loading: false,
    loaded: false,
    photos: [],
    photoData: {},
    errors: []
};

const reducer = ( state = initialState, action) => {
    switch( action.type ){

        case GET_DATA_REQUESTED:
            return {
                ...state,
                loading: true
            };

        case GET_PHOTOS_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                photos: action.data
            };

        case GET_PHOTOS_ERROR:
            return {
                ...state,
                loading: false,
                loaded: false,
                errors: action.data
            };

        case GET_DATA_ITEM:
            return {
                ...state,
                photoData: action.data
            };

        case SET_RATE_REQUESTED:
            return {
                ...state,
                loaded: true
            };

        case SET_PHOTOS_RATE_SUCCESS:
            return (
                {...state,
                    photoData: Object.assign(
                        {...state.photoData}, action.data
                    )
                }
            );

        default:
            return state;
    }
};

export default reducer;