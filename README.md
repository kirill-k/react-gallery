# react-gallery

Created a React component which shows an image gallery

TO START: 

BE: 
D:\react-gallery\gallery-backend

$ yarn
$ SET PORT=3001 node bin/www
$ SET DEBUG=gallery-backend:* & npm start

FE: 
D:\react-gallery\gallery-ui

$ yarn
$ yarn start