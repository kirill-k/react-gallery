const express = require('express') ;
const router = express.Router();
const fs = require('fs');


/* GET users listing. */
router.get('/', (req, res, next) => {
  fs.readFile('./public/data.json', 'utf8', (err, jsonString) => {
    if (err) {
      console.log('Error reading file:', err)
      return
    }
    const gallery = JSON.parse(jsonString)
    res.json(gallery);
  });
});


module.exports = router;