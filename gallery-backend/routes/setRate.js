const express = require('express');
const fs = require('fs');
const router = express.Router();
const fileName = '../public/data.json';
const file = require(fileName);

file.key = "new value";

router.put('/', (req, res) => {

    fs.readFile('./public/data.json', 'utf8', (err, jsonString) => {
        if (err) {
            console.log('Error reading file:',err)
            return
        }

        let id = req.body.id;
        let rate = req.body.rate;
        if (id && rate){
            const success = { "success": "true" };
            res.send(success);
            const gallery = JSON.parse(jsonString)

            const objIndex = gallery.findIndex(obj => obj.id === id);
            const resultObj = gallery.find(x => x.id === id);
            const changeRate = { ...resultObj, rate};

            const updatedGallery = [
                ...gallery.slice(0, objIndex),
                changeRate,
                ...gallery.slice(objIndex + 1),
            ];
            const objToJson = JSON.stringify(updatedGallery);

            fs.writeFile('./public/data.json', objToJson, 'utf8', (err) => {
                if (err) console.log("Error writing file:", err)
            });
        } else {
            const error = "Error 500: Invalid params";
            res.send(error);
        }
    });
});

module.exports = router;